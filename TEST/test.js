var natUpnp = require('nat-upnp');
var http = require("http")
var client = natUpnp.createClient();
var port = 666
client.portMapping({
   public: port,
   private: 2312,
   ttl: 20
}, (err) => {
   //console.log(err)
});

client.getMappings(function (err, results) {
   //console.log(results)
});

client.externalIp(function (err, ip) {
   console.log(ip)
});

const server = http.createServer((req, res) => {
   res.statusCode = 200;
   res.setHeader('Content-Type', 'text/plain');
   res.end('Primer servidor con Node.Js');
});

server.listen(port = 2312, () => {
   console.log('Servidor corriendo ');
});