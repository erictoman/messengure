const dgram = require('dgram');
const UdpHolePuncher = require('udp-hole-puncher');

// peer's public port and address
const peer = {
  port: 5678,
  addr: '189.131.72.191',
};
// local port
const myPort = 5678;

// socket config
const socket = dgram.createSocket('udp4');
socket.on('error', (error) => {

});
socket.on('message', (message, rinfo) => {

});
socket.on('listening', () => {
  // puncher config
  const puncher = new UdpHolePuncher(socket);
  // when connection is established, send dummy message
  puncher.on('connected', () => {
    const message = new Buffer('hello');
    socket.send(message, 0, message.length, peer.port, peer.addr);
  });
  // error handling code
  puncher.on('error', (error) => {

  });
  // connect to peer (using its public address and port)
  puncher.connect(peer.addr, peer.port);
});

// bind socket
socket.bind(myPort);